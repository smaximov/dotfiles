My dotfiles

# Installation

## user-dirs

Copy/link files under `user-dirs` folder to `~/.config`.

## npm

Copy/link `npm/config` to the per-user config file (defaults to `~.npmrc`,
but can be overrided using the `NPM_CONFIG_USERCONFIG` environment variable;
a nice candiate is `$XDG_CONFIG_HOME/npm/config`, which I use).

## mpv

Copy/link `mpv` folder to `~/.config/mpv`.

### MPRIS support

Build [MPRIS plugin](https://github.com/hoyon/mpv-mpris) and put `mpris.so` to `~/.config/mpv/scripts`.

Depepndencies (Fedora):

* `mpv-libs-devel`;
* `glib2-devel`.

## mime

Copy/link `mime/mimeapps.list` to `~/.config`.

## Ruby

Copy/link `ruby/pryrc` to `~/.pryrc`

## PostgreSQL

Copy/link `psql/psqlrc` to `~/.psqlrc`
