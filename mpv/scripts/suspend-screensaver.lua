require 'mp.msg'
local utils = require 'mp.utils'

local XDG_SCREENSAVER_COMMAND = {
   args = { "xdg-screensaver", "reset" }
}
local TIMER_INTERVAL = 30       -- 30 seconds

local function suspend_screensaver()
   local active = not mp.get_property_native('core-idle')
   local fullscreen = mp.get_property_native('fullscreen')

   if active or fullscreen then
      local result = utils.subprocess(XDG_SCREENSAVER_COMMAND, false)

      if result.error ~= nil then
         mp.msg.error('xdg-suprocess failed:', result.status, result.error)
      end
   end
end

mp.add_periodic_timer(TIMER_INTERVAL, suspend_screensaver)
